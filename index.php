<?php
//require_once 'includes.php';
    $inputSize = 0;
    $inputWeight = 0;
    $bmi = 0;


    if (isset($_GET["submit"])){
        $inputSize = filter_input(INPUT_GET, "inputSize", FILTER_VALIDATE_INT);
        $inputWeight = filter_input(INPUT_GET, "inputWeight", FILTER_VALIDATE_INT);

        $weight = $inputWeight;
        $size = ($inputSize / 100) * ($inputSize / 100);
        $bmi=  round($weight / $size);

        if( $bmi >= 40){
            $weightStatus = "Fettleibige Klasse III (sehr stark fettleibig)";
        }else if( $bmi >= 35 && $bmi < 40){
            $weightStatus = "Fettleibige Klasse II (stark fettleibig)";
        }else if( $bmi >= 30 && $bmi < 35){
            $weightStatus = "Fettleibige Klasse I (mäßig fettleibig)";
        }else if( $bmi >= 25 && $bmi < 30){
            $weightStatus = "Übergewicht";
        }else if( $bmi >= 18.5 && $bmi < 25){
            $weightStatus = "Normal (gesundes Gewicht)";
        }
        else if( $bmi >= 16 && $bmi < 18.5){
            $weightStatus = "Untergewicht";
        }
        else if( $bmi >= 15 && $bmi < 16){
            $weightStatus = "Extrem untergewichtig";
        }
        else if( $bmi < 15){
            $weightStatus = "Sehr stark untergewichtig";
        }
    }
       
    

?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>This is the title. It is displayed in the titlebar of the window in most browsers.</title>
    <meta name="description" content="Here is a short description for the page. This text is displayed e. g. in search engine result listings.">
    <!-- Bootstrap -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/add.css" rel="stylesheet"> <!-- You could add your own css-definitions in a file - but you have to create it first... -->
    <!-- The following css-definitions are used just for showing you where the components of this page are placed. Feel free to delete the whole style-tag and to remove the classes in the html elements. -->
    <style>
        .bg-color01 {
            background-color: #AAA;
        }
        .bg-color02 {
            background-color: #9BC;
        }
        .bg-color03 {
            background-color: #579;
        }
        div[class*='level'] {
            border: 1px solid black;
            margin: 2px;
        }
    </style>
    <!--[if lt IE 9]>
      <script src="./js/html5shiv.min.js"></script>
      <script src="./js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <header class="container">
        <p>The header is usually the place for a logo or a picture, a navigation bar or a search field.</p>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Solve IT yourself!</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Home <span class="sr-only">(current)</span></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="polish-vet.php">Polish VET at Zespół Szkół Nr 2 w Milanówku</a></li>
                                <li><a href="german-vet.php">German VET at Eckener-Schule Flensburg</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="overview.php">Project-overview</a></li>
                            </ul>
                        </li>
                        <li><a href="location.php">Find us</a></li>
                        <li><a href="contact.php"><span class="glyphicon glyphicon-envelope"></span>  Contact us</a></li>
                        <li><a href="policies-and-legislation.php">Policies and legislation</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </header>
    <section class="container bg-color01">
        <h1>Hi, developers!</h1>
        <p>Here comes the content..</p>
        <p class="jumbotron">Fascinating - another design for this paragraph with Bootstrap! <span class="glyphicon glyphicon-thumbs-up"></span></p>

        <form method="GET">


            <label for="inputSize">Größe</label>
            <br>
            <input type="number" name="inputSize" id="inputSize" value=<?php echo $inputSize; ?> > 
            <br>
            <label for="inputWeight">Gewicht</label>
            <br>
            <input type="number" name="inputWeight" id="inputWeight" value=<?php echo $inputWeight; ?> >
            <br><br>

            <input type="submit" name="submit" value="Berechnen">

           
        
        </form>

        <p><?php 
            if($bmi > 0 ){
                echo 'BMI: '.$bmi.'<br>'.$weightStatus;                 
            }
        ?>
        </p>

        <article id="tree"> <!-- this article is filled with example-data -->
            <div class="row level1">
                <div id="level1-content" class="col-md-12">
                    <h2>Level 1: Information and instructions...</h2>
                    <p>Filled by jQuery and AJAX.</p>
                </div>
                <div id="level1-q-and-a" class="col-md-12">
                    The form and it's data.<br />
                    Filled by jQuery and AJAX.
                </div>
            </div>
            <div class="row level2">
                <div id="level2-content" class="col-md-12">
                    <h2>Level 2: Information and instructions...</h2>
                    <p>Filled by jQuery and AJAX.</p>
                </div>
                <div id="level2-q-and-a" class="col-md-12">
                    The form and it's data.<br />
                    Filled by jQuery and AJAX.
                </div>
            </div>
        </article>
        
    </section>
    <footer class="container bg-color02">
        <p>And here is some text in the footer. And always make sure your html is valid here: <br /><a class="btn btn-link btn-sm active" role="button" href="https://validator.w3.org/#validate_by_input" target="_blank">https://validator.w3.org/#validate_by_input</a></p>
    </footer>
    <script src="./js/jquery.min.js"></script>
    <script src="./js/bootstrap.js"></script>
    <script src="./js/script.js"></script> <!-- Place your javascript here... -->
</body>
</html>